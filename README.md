# NRF52

Nordic Radio's NRF52832 is a nice little chip -- It has a Cortex M4 running at 64 MHz, a 2.4 GHz RF transceiver, 512 kb of flash, a 12 bit 200 ksps ADC, 5x 32 bit timers, a flexible peripheral interconnect system with DMA, and lots of other bells and whistles.

In particular, the chip performs very well for its cost on tests measuring the speed of information transfer in and out of the CPU and over the RF channel.  https://pub.pages.cba.mit.edu/ring/

This page describes how to use the NRF52(832) on Linux/Unix systems with open source tools. 

## Programming the NRF52

Programming involves two steps: 1) compiling code, and 2) flashing the code to the chip.

Thanks to the folks at Adafruit, you can use the Arduino IDE to perform both steps.  Instruction on installing the appropriate libraries can be found <a href='https://learn.adafruit.com/bluefruit-nrf52-feather-learning-guide'>here</a>.

You can also compile and flash the code directly using the same toolchain employed by the IDE.  To do this, you can follow the instructions on <a href='https://pcbreflux.blogspot.com/2016/09/nrf52832-first-steps-with-st-link-v2.html'>this page</a> (starting at heading "GNU ARM Compiler").

The steps for flashing your code depends on what board and programmer you have.

## Adafruit Feather NRF52 Board

This is a great board; I would highly recommend it for NRF52 projects.

This board comes flashed with a Serial DFU bootloader and it has an onboard USB-to-Serial converter.  This allows you to program it simply with a USB cable.

## Custom boards

If application requirements don't allow you to use the Feather board from Adafruit, you can also build custom boards around the available NRF52 modules.

The Adafruit board uses the <a href='http://www.raytac.com/download/MDBT42/MDBT42Q%20spec-Version%20C.pdf'>Raytac MDBT42Q</a>, available from Digikey (PN 1597-1434-ND) for $14.

Fanstel also makes a variety of modules with the NRF52832 as well as the NRF52840 (with USB 2.0 support).  The <a href='http://www.fanstel.com/bt832-1/'>BC-832</a> is a very small module (7.8mm x 8.8mm) available for $7.60 in quantity 10.  The <a href='http://www.fanstel.com/bt832-1-1/'>BT-832</a> is slightly larger, but comes in a variety ranges and sells for just $5 in quantity 10.

Below is a custom board using the Fanstel BC832 module.  It is just 23mm x 18mm, and is designed to use a serial DFU bootloader for programming with a standard FTDI cable.

<img src='bc832-ftdi/bc832-nrf-ftdi-layout.png' width=35%>
<img src='bc832-ftdi/board.jpg' width=55%>

<a href='bc832-ftdi/bc832-nrf-ftdi-traces.png'>BC832 Traces</a>
<a href='bc832-ftdi/bc832-nrf-ftdi-interior.png'>BC832 Interior</a>

For a version using only the Fab Lab inventory parts, sett 
<a href='bc832-ftdi/bc832-nrf-ftdi-fab-layout.png'>FAB-BC832 Layout</a>
<a href='bc832-ftdi/bc832-nrf-ftdi-fab-traces.png'>FAB-BC832 Traces</a>
<a href='bc832-ftdi/bc832-nrf-ftdi-fab-interior.png'>FAB-BC832 Interior</a>

Making a custom board requires programming via Serial Wire Debug (SWD), at least for the bootloader.  Two methods for this are described below.


## Programming with Segger J-Link

The Segger J-Link is a very helpful tool, but it's quite expensive.  With it, we can use Nordic's command line utilities, including `nrfjprog`.  I wrote a shell script with three commands to erase, flash the adafruit bootloader, and then start the chip running.  

```
nrfjprog -f nrf52 --eraseall
nrfjprog -f nrf52 --program feather52_bootloader_v050_s132_v201.hex
nrfjprog -f nrf52 --run
```

<img src='jlink/jlink-wiring.jpg' width=22%>
<img src='jlink/flashing-bootloader.png' width=64%>

The J-Link requires a wire to ensure the target has power.  In the image above, red is target power (3.3V), black is ground, green is SWDCLK, and blue is SWDIO.

## Programming with OpenOCD and Raspberry Pi

You don't have to spend hundreds on a J-Link (even though it is very nice).  Using <a href='openocd.org'>OpenOCD</a>, we can make a Raspberry Pi bit-bang the programming protocols.  Below is a picture and screenshot of using OpenOCD and Pi Zero ($5) to program a custom board with a Raytac MDBT42Q module over serial wire debug.  It works just as well to program the Fanstel modules.

Rough instructions for creating an openOCD programmer using a Raspberry Pi are available on <a href='openocd/RaspberryPi.md'>this page</a>.

<img src='openocd/bootloader-raspberrypi.jpg' width=53%>
<img src='openocd/flashing-bootloader.png' width=30%>

<a href='https://learn.adafruit.com/programming-microcontrollers-using-openocd-on-raspberry-pi/overview'>This page</a> is a very helpful introduction to programming using OpenOCD and the BCM2835.  

Support for the NRF52 is not in the stable release of OpenOCD as of V 0.10, but you can patch it following the instructions on <a href='https://primalcortex.wordpress.com/2017/06/10/setting-up-openocd-for-programming-the-nordic-nrf52832-chip/'>this page</a>.  They worked for me, with the small addition that I needed to eliminate two duplicate variable definitions that broke my build.

I put up a mirror for this patched version <a href='https://gitlab.cba.mit.edu/calischs/openocd_nrf52_patch'>here</a>.  Email sam.calisch@cba.mit.edu for access.

The `openocd.cfg` will also be different for the NRF52 than the linked tutorial.  Here is mine for flashing a binary of the Adafruit bootloader using a Raspberry Pi Zero:
```
source [find interface/raspberrypi-native.cfg]
transport select swd
source [find target/nrf52.cfg]
bcm2835gpio_swd_nums 25 24
bcm2835gpio_trst_num 7
bcm2835gpio_srst_num 18
 
init
targets
reset halt
nrf52 mass_erase 0
program feather52_bootloader_v050_s132_v201.hex verify
reset
shutdown
```

## Hello worlds

* <a href='modules/spi'>SPI (Serial Peripheral Interface)</a>

* <a href='modules/twi'>TWI (i.e., I2C)</a>

* <a href='modules/uarte'>UARTE (i.e., UART with DMA)</a>

* <a href='modules/pwm'>PWM (Pulse width modulation)</a>

* <a href='modules/radio'>2.4 GHz radio (user defined packets)</a>

* <a href='modules/saadc'>SAADC (Successive approximation ADC)</a>

* <a href='modules/timer_gpiote'>Connect Timer to GPIOTE with PPI</a>

* <a href='modules/XL_as_gpio'>Disconnect XL1/XL2 from clock to use as GPIO</a>


