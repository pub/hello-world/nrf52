//sec, 2017
#define N_SAMPLES 48
static int16_t data_buffer[N_SAMPLES] = {0};

void adc_setup() {
  //configure SAADC resolution
  NRF_SAADC->RESOLUTION = SAADC_RESOLUTION_VAL_12bit;
  //configure sample rate via internal timer
  //NRF_SAADC->SAMPLERATE = (( SAADC_SAMPLERATE_MODE_Timers << SAADC_SAMPLERATE_MODE_Pos) & SAADC_SAMPLERATE_MODE_Msk) |
  //                        (( 2047 << SAADC_SAMPLERATE_CC_Pos) & SAADC_SAMPLERATE_CC_Msk);
  //enable oversampling -- do not combine with scan mode!
  //NRF_SAADC->OVERSAMPLE = (SAADC_OVERSAMPLE_OVERSAMPLE_Over256x << SAADC_OVERSAMPLE_OVERSAMPLE_Pos)  & SAADC_OVERSAMPLE_OVERSAMPLE_Msk ;
  NRF_SAADC->OVERSAMPLE = (SAADC_OVERSAMPLE_OVERSAMPLE_Bypass << SAADC_OVERSAMPLE_OVERSAMPLE_Pos)  & SAADC_OVERSAMPLE_OVERSAMPLE_Msk ;
  //enable SAADC
  NRF_SAADC->ENABLE = (SAADC_ENABLE_ENABLE_Enabled << SAADC_ENABLE_ENABLE_Pos);
  //set result pointer
  NRF_SAADC->RESULT.PTR = (uint32_t)(&data_buffer);
  NRF_SAADC->RESULT.MAXCNT = N_SAMPLES; // number of samples

  //set channel 0 resistor network, gain, reference, sample time, and mode
  NRF_SAADC->CH[0].CONFIG =   ((SAADC_CH_CONFIG_RESP_Bypass     << SAADC_CH_CONFIG_RESP_Pos)   & SAADC_CH_CONFIG_RESP_Msk)
                            | ((SAADC_CH_CONFIG_RESP_Bypass     << SAADC_CH_CONFIG_RESN_Pos)   & SAADC_CH_CONFIG_RESN_Msk)
                            | ((SAADC_CH_CONFIG_GAIN_Gain4      << SAADC_CH_CONFIG_GAIN_Pos)   & SAADC_CH_CONFIG_GAIN_Msk)
                            | ((SAADC_CH_CONFIG_REFSEL_Internal << SAADC_CH_CONFIG_REFSEL_Pos) & SAADC_CH_CONFIG_REFSEL_Msk)
                            | ((SAADC_CH_CONFIG_TACQ_5us        << SAADC_CH_CONFIG_TACQ_Pos)   & SAADC_CH_CONFIG_TACQ_Msk)
                            | ((SAADC_CH_CONFIG_BURST_Disabled  << SAADC_CH_CONFIG_BURST_Pos)   & SAADC_CH_CONFIG_BURST_Msk)
                            | ((SAADC_CH_CONFIG_MODE_Diff       << SAADC_CH_CONFIG_MODE_Pos)   & SAADC_CH_CONFIG_MODE_Msk);
  //configure Channel 0 to use A0 as positive, A1 as negative
  NRF_SAADC->CH[0].PSELP = SAADC_CH_PSELP_PSELP_AnalogInput0; 
  NRF_SAADC->CH[0].PSELN = SAADC_CH_PSELP_PSELP_AnalogInput1; 
  
  //set channel 1 resistor network, gain, reference, sample time, and mode
  NRF_SAADC->CH[1].CONFIG = ((SAADC_CH_CONFIG_RESP_Bypass       << SAADC_CH_CONFIG_RESP_Pos)   & SAADC_CH_CONFIG_RESP_Msk)
                            | ((SAADC_CH_CONFIG_RESP_Bypass     << SAADC_CH_CONFIG_RESN_Pos)   & SAADC_CH_CONFIG_RESN_Msk)
                            | ((SAADC_CH_CONFIG_GAIN_Gain4    << SAADC_CH_CONFIG_GAIN_Pos)   & SAADC_CH_CONFIG_GAIN_Msk)
                            | ((SAADC_CH_CONFIG_REFSEL_Internal << SAADC_CH_CONFIG_REFSEL_Pos) & SAADC_CH_CONFIG_REFSEL_Msk)
                            | ((SAADC_CH_CONFIG_TACQ_5us        << SAADC_CH_CONFIG_TACQ_Pos)   & SAADC_CH_CONFIG_TACQ_Msk)
                            | ((SAADC_CH_CONFIG_BURST_Disabled   << SAADC_CH_CONFIG_BURST_Pos)   & SAADC_CH_CONFIG_BURST_Msk)
                            | ((SAADC_CH_CONFIG_MODE_Diff         << SAADC_CH_CONFIG_MODE_Pos)   & SAADC_CH_CONFIG_MODE_Msk);
  //configure Channel 1 to use A2 as positive, A4 as negative
  NRF_SAADC->CH[1].PSELP = SAADC_CH_PSELP_PSELP_AnalogInput2; 
  NRF_SAADC->CH[1].PSELN = SAADC_CH_PSELP_PSELP_AnalogInput4; 
}

void setup(){
  //start task
  NRF_SAADC->TASKS_START = 0x01UL;
  while (!NRF_SAADC->EVENTS_STARTED); NRF_SAADC->EVENTS_STARTED = 0x00UL;
  for(int i=0; i<N_SAMPLES; i++){
    //sample task channel 0
    NRF_SAADC->TASKS_SAMPLE = 0x01UL;
    while (!NRF_SAADC->EVENTS_DONE); NRF_SAADC->EVENTS_DONE = 0x00UL;
    //sample task channel 1
    NRF_SAADC->TASKS_SAMPLE = 0x01UL;
    while (!NRF_SAADC->EVENTS_DONE); NRF_SAADC->EVENTS_DONE = 0x00UL;
  } 
  NRF_SAADC->TASKS_STOP = 0x01UL;
  while (!NRF_SAADC->EVENTS_STOPPED); NRF_SAADC->EVENTS_STOPPED = 0x00UL;    
}

void loop() {}
