// Will Langford
// Jan 16, 2018
// Basic example comparing the Adafruit Feather Serial implementation vs Uart
// the conclusioni is essentially that they're the same (if you're sending a character string anyway)
// Serial.print is non-blocking (just like the uart_send() function)
// UNLESS: you're writing data to the UART faster than it can send out... then it will block your code

#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>

#define n_tx_bytes 20 //only as many as needed
#define n_rx_bytes 20 //only as many as needed
const uint8_t pin_rx = 8;
const uint8_t pin_tx = 6;
static char uart_tx_data[n_tx_bytes] = {0};
static char uart_rx_data[n_rx_bytes] = {0};

volatile bool timer2_triggered = false;

const uint8_t enablePin = 27;

uint16_t counter = 0;

extern "C" // for some strange reason this seems necessary for the interrupt to function
{
  void TIMER2_IRQHandler(void)
  {
      // Clear events
      NRF_TIMER2->EVENTS_COMPARE[0] = 0;

      timer2_triggered = true;
  }
}

void writeToBuffer(char *buff, char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vsnprintf(buff, n_tx_bytes, fmt, args);
    va_end(args);
}

void uart_setup() {
  //uart with dma
  NRF_UARTE0->PSEL.TXD = (pin_tx << UARTE_PSEL_TXD_PIN_Pos) & UARTE_PSEL_TXD_PIN_Msk;
  NRF_UARTE0->PSEL.RXD = (pin_rx << UARTE_PSEL_RXD_PIN_Pos) & UARTE_PSEL_RXD_PIN_Msk;
  NRF_UARTE0->CONFIG =  ((UART_CONFIG_PARITY_Excluded << UARTE_CONFIG_PARITY_Pos) & UARTE_CONFIG_PARITY_Msk) 
                      | ((UARTE_CONFIG_HWFC_Disabled << UARTE_CONFIG_HWFC_Pos) & UARTE_CONFIG_HWFC_Msk);
  NRF_UARTE0->BAUDRATE = UART_BAUDRATE_BAUDRATE_Baud115200;
  NRF_UARTE0->ENABLE = (UARTE_ENABLE_ENABLE_Enabled << UARTE_ENABLE_ENABLE_Pos) & UARTE_ENABLE_ENABLE_Msk;
  
  NRF_UARTE0->TXD.MAXCNT = n_tx_bytes;
  NRF_UARTE0->RXD.MAXCNT = n_rx_bytes;
}

void uart_send() {
  NRF_UARTE0->EVENTS_ENDTX = 0;
  NRF_UARTE0->TXD.PTR = (uint32_t)(&uart_tx_data); //reset pointer to start of buffer
  NRF_UARTE0->TASKS_STARTTX = 1;  //trigger start task to send data to host
}

void timer2_init()
{      
    NVIC_EnableIRQ(TIMER2_IRQn);
    NVIC_ClearPendingIRQ(TIMER2_IRQn);
  
    NRF_TIMER2->MODE = TIMER_MODE_MODE_Timer;       // Set the timer in Timer Mode.
    NRF_TIMER2->PRESCALER = 9;                        
    NRF_TIMER2->BITMODE = TIMER_BITMODE_BITMODE_16Bit; // 16 bit mode.
    NRF_TIMER2->TASKS_CLEAR = 1;
    NRF_TIMER2->CC[0] = 632; // with prescaler 9, this triggers at 200 Hz
    NRF_TIMER2->EVENTS_COMPARE[0] = 0;
    NRF_TIMER2->SHORTS = (TIMER_SHORTS_COMPARE0_CLEAR_Enabled << TIMER_SHORTS_COMPARE0_CLEAR_Pos);

    NRF_TIMER2->INTENSET = TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos;
    NVIC_EnableIRQ(TIMER2_IRQn);
}

void timer2_start() {
  NRF_TIMER2->TASKS_START = 1;
}

void setup() {
//  Serial.begin(115200);
  uart_setup();
  
  NRF_GPIO->DIRSET = (1 << enablePin);
  NRF_GPIO->OUTSET = (1 << enablePin);

  timer2_init();
  timer2_start();

}

void loop() { 
  if (timer2_triggered) {

    // UARTE
    writeToBuffer(&uart_tx_data[0],"count=%d \n", counter);
    uart_send();

    // Serial
//    Serial.print("count=");
//    Serial.println(counter);

    counter++;

    timer2_triggered = false;
  }
}
