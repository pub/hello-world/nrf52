// Will Langford
// Jan 12, 2018
// Basic example showing how to use the SAADC with oversampling and interrupts
// In this code we:
//    set up the SAADC to oversample 256x in a single-ended configuration on Analog 0 (Pin 0.2)
//    enable burst mode so that we only need to trigger 1 sample to get a fully sampled (256x) result
//    enable an interrupt on the SAADC END_EVENT which triggers whenever the result buffer is full

static int16_t data_buffer[1];
volatile bool saadc_results_ready = false;

extern "C" // for some strange reason this seems necessary for the interrupt to function
{
  void SAADC_IRQHandler(void)
  {
      // Clear events
      NRF_SAADC->EVENTS_END = 0;
      saadc_results_ready = true;
  }
}

void adc_setup() {
  NVIC_EnableIRQ(SAADC_IRQn);
  NVIC_ClearPendingIRQ(SAADC_IRQn);
  //configure SAADC resolution
  NRF_SAADC->RESOLUTION = SAADC_RESOLUTION_VAL_14bit;

  // enable oversampling
  NRF_SAADC->OVERSAMPLE = (SAADC_OVERSAMPLE_OVERSAMPLE_Over256x << SAADC_OVERSAMPLE_OVERSAMPLE_Pos) & SAADC_OVERSAMPLE_OVERSAMPLE_Msk ;

  //enable SAADC
  NRF_SAADC->ENABLE = (SAADC_ENABLE_ENABLE_Enabled << SAADC_ENABLE_ENABLE_Pos);
  
  //set result pointer
  NRF_SAADC->RESULT.PTR = (uint32_t)(&data_buffer);
  NRF_SAADC->RESULT.MAXCNT = 1; // number of samples

  for (int i = 0; i < 8; i++) {
    NRF_SAADC->CH[i].PSELN = SAADC_CH_PSELP_PSELP_NC;
    NRF_SAADC->CH[i].PSELP = SAADC_CH_PSELP_PSELP_NC;
  }

  //set channel 0 resistor network, gain, reference, sample time, and mode
  NRF_SAADC->CH[0].CONFIG =   ((SAADC_CH_CONFIG_RESP_Bypass     << SAADC_CH_CONFIG_RESP_Pos)   & SAADC_CH_CONFIG_RESP_Msk)
                            | ((SAADC_CH_CONFIG_RESP_Bypass     << SAADC_CH_CONFIG_RESN_Pos)   & SAADC_CH_CONFIG_RESN_Msk)
                            | ((SAADC_CH_CONFIG_GAIN_Gain1_4    << SAADC_CH_CONFIG_GAIN_Pos)   & SAADC_CH_CONFIG_GAIN_Msk)
                            | ((SAADC_CH_CONFIG_REFSEL_VDD1_4   << SAADC_CH_CONFIG_REFSEL_Pos) & SAADC_CH_CONFIG_REFSEL_Msk)
                            | ((SAADC_CH_CONFIG_TACQ_5us        << SAADC_CH_CONFIG_TACQ_Pos)   & SAADC_CH_CONFIG_TACQ_Msk)
                            | ((SAADC_CH_CONFIG_BURST_Enabled   << SAADC_CH_CONFIG_BURST_Pos)  & SAADC_CH_CONFIG_BURST_Msk)
                            | ((SAADC_CH_CONFIG_MODE_SE         << SAADC_CH_CONFIG_MODE_Pos)   & SAADC_CH_CONFIG_MODE_Msk);
                            
  //configure Channel 0 to use A0 as positive
  NRF_SAADC->CH[0].PSELP = SAADC_CH_PSELP_PSELP_AnalogInput0; 
  NRF_SAADC->CH[0].PSELN = SAADC_CH_PSELP_PSELP_AnalogInput0; 


  // Enable SAADC END interrupt to do maintainance and printing of values. 
  NRF_SAADC->INTENSET = SAADC_INTENSET_END_Enabled << SAADC_INTENSET_END_Pos;
  NVIC_EnableIRQ(SAADC_IRQn);
}

void adc_start()
{
    // Enable SAADC. This should be done after the SAADC is configure due to errata 74 SAADC: Started events fires prematurely
    NRF_SAADC->ENABLE = (SAADC_ENABLE_ENABLE_Enabled << SAADC_ENABLE_ENABLE_Pos);
    
    //start task
    NRF_SAADC->TASKS_START = 0x01UL;
    while (!NRF_SAADC->EVENTS_STARTED); NRF_SAADC->EVENTS_STARTED = 0x00UL;
    NRF_SAADC->TASKS_SAMPLE = 0x01UL;
}


void setup() {

  Serial.begin(115200);
  Serial.println("started...");
  
  adc_setup();
  adc_start();
  
  Serial.println("adc started...");
}

void loop() { 
  if (saadc_results_ready) {
    Serial.println(data_buffer[0]);
    
    // restart acquistion
    NRF_SAADC->TASKS_START = 0x01UL;
    while (!NRF_SAADC->EVENTS_STARTED); NRF_SAADC->EVENTS_STARTED = 0x00UL;
    NRF_SAADC->TASKS_SAMPLE = 0x01UL;
    saadc_results_ready = false;
  }
}
