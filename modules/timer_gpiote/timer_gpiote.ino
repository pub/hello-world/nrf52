//sec, 2017
//adapted from https://devzone.nordicsemi.com/question/44628/maximum-clock-possible-on-gpio-pin-nrf51/

#define TEST_PIN               0 //XL1
#define GPIOTE_CHANNEL_NUMBER  0 

void gpiote_init()
{
    //configure output and drive
    NRF_GPIO->PIN_CNF[TEST_PIN] = 
          ((GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos) & GPIO_PIN_CNF_DIR_Msk) | 
          ((GPIO_PIN_CNF_INPUT_Disconnect << GPIO_PIN_CNF_INPUT_Pos) & GPIO_PIN_CNF_INPUT_Msk) | 
          ((GPIO_PIN_CNF_DRIVE_S0S1 << GPIO_PIN_CNF_DRIVE_Pos) & GPIO_PIN_CNF_DRIVE_Msk);
    NRF_GPIO->OUTCLR = (1 << TEST_PIN);  //low initial value

    NRF_GPIOTE->TASKS_OUT[GPIOTE_CHANNEL_NUMBER] = 1;
    NRF_GPIOTE->CONFIG[GPIOTE_CHANNEL_NUMBER] = (GPIOTE_CONFIG_MODE_Task << GPIOTE_CONFIG_MODE_Pos) |
                               (TEST_PIN << GPIOTE_CONFIG_PSEL_Pos) |
                               (GPIOTE_CONFIG_POLARITY_Toggle << GPIOTE_CONFIG_POLARITY_Pos) |
                               (GPIOTE_CONFIG_OUTINIT_Low << GPIOTE_CONFIG_OUTINIT_Pos);
    __NOP(); __NOP(); __NOP(); 
}

void timer2_init()
{      
    NRF_TIMER2->MODE = TIMER_MODE_MODE_Timer;       // Set the timer in Timer Mode.
    NRF_TIMER2->PRESCALER = 0;                          
    NRF_TIMER2->BITMODE = TIMER_BITMODE_BITMODE_16Bit; // 16 bit mode.
    NRF_TIMER2->TASKS_CLEAR = 1;
    NRF_TIMER2->CC[0] = 1;
    NRF_TIMER2->EVENTS_COMPARE[0] = 0;
    NRF_TIMER2->SHORTS = (TIMER_SHORTS_COMPARE0_CLEAR_Enabled << TIMER_SHORTS_COMPARE0_CLEAR_Pos);
}

void ppi_init()
{
    NRF_PPI->CH[0].EEP = (uint32_t)&NRF_TIMER2->EVENTS_COMPARE[0];
    NRF_PPI->CH[0].TEP = (uint32_t)&NRF_GPIOTE->TASKS_OUT[GPIOTE_CHANNEL_NUMBER];
    NRF_PPI->CHEN = (PPI_CHEN_CH0_Enabled << PPI_CHEN_CH0_Pos);     // Enable PPI channel 0
}

void setup(){
    //Need to switch to internal LFCLK to disconnect from XL1 and XL2
    NRF_CLOCK->LFCLKSRC = 0; //disconnect XL1 AND XL2 FROM LFCLK
    NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_LFCLKSTART    = 1;
    while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0){}

    timer2_init();
    ppi_init();
    gpiote_init();
    NRF_TIMER2->TASKS_START = 1;
    while(1){};
    //this should output a 8MHz square wave on TEST_PIN
    
}

void loop(){}

