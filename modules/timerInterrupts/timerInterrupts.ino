// Will Langford
// Setting up a timer with an interrupt handler

const uint8_t enablePin = 27;

volatile bool timer2_triggered = false;

uint32_t counter = 0;

extern "C" // for some strange reason this seems necessary for the interrupt to function
{
  void TIMER2_IRQHandler(void)
  {
      // Clear events
      NRF_TIMER2->EVENTS_COMPARE[0] = 0;
      timer2_triggered = true;
  }
}

void timer2_init()
{      
    NVIC_EnableIRQ(TIMER2_IRQn);
    NVIC_ClearPendingIRQ(TIMER2_IRQn);
  
    NRF_TIMER2->MODE = TIMER_MODE_MODE_Timer;       // Set the timer in Timer Mode.
    NRF_TIMER2->PRESCALER = 9;                        
    NRF_TIMER2->BITMODE = TIMER_BITMODE_BITMODE_16Bit; // 16 bit mode.
    NRF_TIMER2->TASKS_CLEAR = 1;
    NRF_TIMER2->CC[0] = 316; // with prescaler 9, this triggers at 100 Hz
    NRF_TIMER2->EVENTS_COMPARE[0] = 0;
    NRF_TIMER2->SHORTS = (TIMER_SHORTS_COMPARE0_CLEAR_Enabled << TIMER_SHORTS_COMPARE0_CLEAR_Pos);

    NRF_TIMER2->INTENSET = TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos;
    NVIC_EnableIRQ(TIMER2_IRQn);
}

void timer2_start() {
  NRF_TIMER2->TASKS_START = 1;
}

void setup() {

  Serial.begin(115200);
  Serial.println("started...");
  
  NRF_GPIO->DIRSET = (1 << enablePin);
  NRF_GPIO->OUTSET = (1 << enablePin);
  
  timer2_init();
  timer2_start();
  
  Serial.println("timer started...");
}

void loop() { 
  
  if (timer2_triggered) {
    //toggle gpio pin
    NRF_GPIO->OUT ^= (1 << enablePin);

    // increment counter and print out every 1000 counts
    counter++;
    if (!(counter % 1000)) { Serial.println(micros()); }
    
    timer2_triggered = false;
  }
  
}
