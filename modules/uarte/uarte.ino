//sec, 2017

#define n_tx_bytes 4 //only as many as needed
#define n_rx_bytes 4 //only as many as needed
const uint8_t pin_rx = 8;
const uint8_t pin_tx = 6;
static uint8_t uart_tx_data[n_tx_bytes] = {0};
static uint8_t uart_rx_data[n_rx_bytes] = {0};

void setup() {
  //uart with dma
  NRF_UARTE0->PSEL.TXD = (pin_tx << UARTE_PSEL_TXD_PIN_Pos) & UARTE_PSEL_TXD_PIN_Msk;
  NRF_UARTE0->PSEL.RXD = (pin_rx << UARTE_PSEL_RXD_PIN_Pos) & UARTE_PSEL_RXD_PIN_Msk;
  NRF_UARTE0->CONFIG =  ((UART_CONFIG_PARITY_Excluded << UARTE_CONFIG_PARITY_Pos) & UARTE_CONFIG_PARITY_Msk) 
                      | ((UARTE_CONFIG_HWFC_Disabled << UARTE_CONFIG_HWFC_Pos) & UARTE_CONFIG_HWFC_Msk);
  NRF_UARTE0->BAUDRATE = UART_BAUDRATE_BAUDRATE_Baud1M;
  NRF_UARTE0->ENABLE = (UARTE_ENABLE_ENABLE_Enabled << UARTE_ENABLE_ENABLE_Pos) & UARTE_ENABLE_ENABLE_Msk;
  
  NRF_UARTE0->TXD.MAXCNT = n_tx_bytes;
  NRF_UARTE0->RXD.MAXCNT = n_rx_bytes;

  //send data
  NRF_UARTE0->EVENTS_ENDTX = 0;
  NRF_UARTE0->TXD.PTR = (uint32_t)(&uart_tx_data); //reset pointer to start of buffer
  NRF_UARTE0->TASKS_STARTTX = 1;  //trigger start task to send data to host
  while(!NRF_UARTE0->EVENTS_ENDTX){}

  //recv data, untested
  //NRF_UARTE0->EVENTS_RXDRDY = 0;
  //NRF_UARTE0->RXD.PTR = (uint32_t)(&uart_rx_data); //reset pointer to start of buffer
  //NRF_UARTE0->TASKS_STARTRX = 1;  //trigger start task to recieve data from host
  //while(!NRF_UARTE0->EVENTS_RXDRDY){}  
}

void loop() {}
