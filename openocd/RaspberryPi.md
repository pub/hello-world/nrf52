## Setting up a Raspberry Pi OpenOCD programmer

1. Set up SD card.  Either download image with openocd from this page (todo: add link), or download a vanilla raspian os from https://www.raspberrypi.org/downloads/raspbian/ This will involve building OpenOCD yourself.  To fit on a 4GB sd card, get the lite version.

1. https://www.raspberrypi.org/documentation/installation/installing-images/

```
diskutil unmountDisk diskn
sudo dd bs=1m if=~/Documents/2017-09-07-raspbian-stretch-lite.img of=/dev/rdiskn conv=sync
```

Wait.

2. Set up ssh.  We prefer over usb: https://www.thepolyglotdeveloper.com/2016/06/connect-raspberry-pi-zero-usb-cable-ssh/
```
sublime /Volumes/boot/config.txt
dtoverlay=dwc2
sublime /Volumes/boot/cmdline.txt
modules-load=dwc2,g_ether
touch /Volumes/boot/ssh
ssh pi@raspberrypi.local
raspberry
```

3. Change password.
```
passwd
```

4. Get OpenOCD with nrf52 patch.
5. Make and install.  You may need to install some things, and you might need to share internet from your PC over the usb connection.
```
sudo apt-get update
sudo apt-get install autoconf automake libtool git
```
```
cd openocd/
./bootstrap
./configure --enable-sysfsgpio --enable-bcm2835gpio
make
sudo make install
```

6. Back up the sd card!  If you corrupt the card, you can skip steps 1-5 if you have a backup.
```
sudo dd if=/dev/diskn of=~/SDCardBackup.dmg
```

7. Get a bootloader.  Adafruit's is nice.
8. Write openocd.cfg.
9. sudo openocd


